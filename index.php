<?php

header("Content-Type:application/json");
header('Access-Control-Allow-Origin: *');
header("Access-Control-Allow-Headers: X-API-KEY, Origin, X-Requested-With, Content-Type, Accept, Access-Control-Request-Method");
header("Access-Control-Allow-Methods: GET, POST, OPTIONS, PUT, DELETE");
header("Allow: GET, POST, OPTIONS, PUT, DELETE");
$method = $_SERVER['REQUEST_METHOD'];
if($method == "OPTIONS") {
    die();
}


$pdo = new PDO('mysql:host=localhost;port=3307;dbname=verificador', 'root', '123456');
$pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

try {

  $codigo = $_GET["codigo"];

  if(!empty($_GET["codigo"])) {

    $statement = $pdo->prepare('SELECT nombre_producto, precio_producto, descripcion_producto, imagen_producto FROM productos WHERE id_producto='.$codigo);
    $statement->execute();
    $producto = $statement->fetch(PDO::FETCH_ASSOC);

    if($producto) {
      peticion(200, $producto);
    } else {
      peticion(400, $codigo);
    }
  } 
} catch (\Throwable $th) {
  return $th;
}


function peticion($status, $data) {

  header("HTTP/1.1".$status);
  $respuesta['status'] = $status;

  if($status===200) {
    $respuesta['nombre'] = $data['nombre_producto']; 
    $respuesta['precio'] = $data['precio_producto']; //precio_producto
    $respuesta['descripcion'] = $data['descripcion_producto'];
    $respuesta['imagen'] = $data['imagen_producto'];
    // imagen_producto
  } else {
    $respuesta['mensaje'] = "No se encontró Producto con codigo={$data}"; 
  }

  $json_response = json_encode($respuesta);
  echo $json_response;
}


?>